This component implements functionality to extend the espidf console module. It 
 provides the following functionality:
   - Command execution: Is pretty self explanatory. However, command execution
       can also be disabled. This comes in handy if there are console listeners
       active.
   - Input forwarding: Input entered on the command can be forwarded to console
       listeners. A console listener provides a queue handle and the module 
       will forward the command to this queue. Multiple console listeners can 
       be registered. However, a single console listener can also request 
       exclusive access. In that case, all other console listneres will not 
       receive input until the exclusive console listener is deregistered.
 
   Original code by Espressif Systems:
       SPDX-FileCopyrightText: 2016-2021 Espressif Systems (Shanghai) CO LTD
       SPDX-License-Identifier: Apache-2.0
  
  Modifications from original code:
   - Names of functions to avoid clash with orginial function names when 
       including esp-idf components.
   - Slight modifications to allow for asynchronous linennoise function (see
       ch405lab_linenoise) to be used.
   - Added possibility to forward console input to subscribers (from concurrent
       threads).
